import chai from "chai";
import chaiHttp from "chai-http";
import {User} from "../src/models/user.js";
import {Task} from "../src/models/task.js";
import {server} from "../src/index.js";

chai.should();
chai.use(chaiHttp);

describe('TaskManager API', () => {
    before(async () => {
        await User.deleteMany({});
        await Task.deleteMany({});
    })
    let tokenTest1 = '';
    let tokenTest2 = '';

    describe("POST /users", () => {
        it("Реєстрація користувача User1 з помилкою валідації", (done => {
            const user = {
                "name": "test2",
                "age": 18,
                "email": "test2gmail.com",
                "password": "123456789"
            };
            chai.request(server)
                .post("/users")
                .send(user)
                .end((err, response) => {
                    response.should.have.status(400);
                    response
                        .body
                        .should
                        .have
                        .property('error')
                        .eq('User validation failed: email: Email is invalid')
                    done();
                })
        }))
        it("Реєстрація користувача User1 без помилок", (done => {
            const user = {
                "name": "test1",
                "age": 18,
                "email": "test1@gmail.com",
                "password": "123456789"
            };
            chai.request(server)
                .post("/users")
                .send(user)
                .end((err, response) => {
                    response.should.have.status(201);
                    response
                        .body
                        .should
                        .have
                        .property('_id');
                    done();
                })
        }))
        it("Реєстрація користувача User2 без помилок", (done => {
            const user = {
                "name": "test2",
                "age": 18,
                "email": "test2@gmail.com",
                "password": "123456789"
            };
            chai.request(server)
                .post("/users")
                .send(user)
                .end((err, response) => {
                    response.should.have.status(201);
                    response
                        .body
                        .should
                        .have
                        .property('_id');
                    done();
                })
        }))
    })

    describe("POST /users/login", () => {
        it("Вхід під User1 з вірними даними", (done => {
            const user = {
                "email": "test1@gmail.com",
                "password": "123456789"
            };
            chai.request(server)
                .post("/users/login")
                .send(user)
                .end((err, response) => {
                    response.should.have.status(200);
                    response
                        .body
                        .should
                        .have
                        .property('token')
                    tokenTest1 = response.body.token;
                    done();
                })
        }))
    });

    let test1Task1Id = ''
    describe("POST /tasks", () => {
        it("Додавання задачі Task1", (done => {
            const task = {
                "description": "test1 Task1",
                "completed": false
            }
            chai.request(server)
                .post("/tasks")
                .set('Authorization', 'Bearer ' + tokenTest1)
                .send(task)
                .end((err, response) => {
                    response.should.have.status(201);
                    response
                        .body
                        .should
                        .have
                        .property('_id')
                    test1Task1Id = response.body._id;
                    done();
                })
        }))
        it("Додавання задачі Task2", (done => {
            const task = {
                "description": "test1 Task2",
                "completed": false
            }
            chai.request(server)
                .post("/tasks")
                .set('Authorization', 'Bearer ' + tokenTest1)
                .send(task)
                .end((err, response) => {
                    response.should.have.status(201);
                    response
                        .body
                        .should
                        .have
                        .property('_id')
                    done();
                })
        }))
    });

    describe("GET /tasks", () => {
        it("Отримання задач користувача User1", (done => {
            chai.request(server)
                .get("/tasks")
                .set('Authorization', 'Bearer ' + tokenTest1)
                .end((err, response) => {
                    response.should.have.status(200);
                    response
                        .body
                        .length
                        .should
                        .be
                        .eq(2)
                    done();
                })
        }))
    });

    describe("GET /tasks/:id", () => {
        it("Отримуємо задачу Task1 по ідентифікатору", (done => {
            chai.request(server)
                .get("/tasks/" + test1Task1Id)
                .set('Authorization', 'Bearer ' + tokenTest1)
                .end((err, response) => {
                    response.should.have.status(200);
                    response
                        .body
                        .should
                        .have
                        .property('description');
                    response
                        .body
                        .should
                        .have
                        .property('completed')
                    done();
                })
        }))
    });

    describe("POST /users/logout", () => {
        it("Вихід", (done => {
            chai.request(server)
                .post('/users/logout')
                .set('Authorization', 'Bearer ' + tokenTest1)
                .end((err, response) => {
                    response.should.have.status(200);
                    done();
                })
        }))
    });

    describe("POST /users/login", () => {
        it("Вхід під User2 з вірними даними", (done => {
            const user = {
                "email": "test2@gmail.com",
                "password": "123456789"
            };
            chai.request(server)
                .post("/users/login")
                .send(user)
                .end((err, response) => {
                    response.should.have.status(200);
                    response
                        .body
                        .should
                        .have
                        .property('token')
                    tokenTest2 = response.body.token;
                    done();
                })
        }))
    });

    describe("POST /tasks", () => {
        it("Додавання задачі Task3", (done => {
            const task = {
                "description": "test2 Task3",
                "completed": false
            }
            chai.request(server)
                .post("/tasks")
                .set('Authorization', 'Bearer ' + tokenTest2)
                .send(task)
                .end((err, response) => {
                    response.should.have.status(201);
                    response
                        .body
                        .should
                        .have
                        .property('_id')
                    done();
                })
        }))
    });

    describe("GET /tasks", () => {
        it("Отримання задач користувача User2", (done => {
            chai.request(server)
                .get("/tasks")
                .set('Authorization', 'Bearer ' + tokenTest2)
                .end((err, response) => {
                    response.should.have.status(200);
                    response
                        .body
                        .length
                        .should
                        .be
                        .eq(1)
                    done();
                })
        }))
    });

    describe("GET /tasks/:id", () => {
        it("Отримуємо задачу Task1 по ідентифікатору", (done => {
            chai.request(server)
                .get("/tasks/" + test1Task1Id)
                .set('Authorization', 'Bearer ' + tokenTest2)
                .end((err, response) => {
                    response.should.have.status(404);
                    response
                        .body
                        .should
                        .have
                        .property('error')
                        .eq("Task not found")
                    done();
                })
        }))
    });

    describe("POST /users/logout", () => {
        it("Вихід", (done => {
            chai.request(server)
                .post('/users/logout')
                .set('Authorization', 'Bearer ' + tokenTest2)
                .end((err, response) => {
                    response.should.have.status(200);
                    done();
                })
        }))
    });

    describe("GET /tasks/:id", () => {
        it("Отримуємо задачу Task1 по ідентифікатору", (done => {
            chai.request(server)
                .get("/tasks/" + test1Task1Id)
                .set('Authorization', 'Bearer ' + tokenTest2)
                .end((err, response) => {
                    response.should.have.status(401);
                    response
                        .body
                        .should
                        .have
                        .property('error')
                        .eq('Unauthorized')
                    done();
                })
        }))
    });
})