import express from 'express';
import {User} from "../models/user.js";
import bodyParser from "body-parser";
import {auth} from "../middleware/auth.js";

const router = new express.Router();
const jsonParser = bodyParser.json()

router.post('/users/login', jsonParser, async (req, res) => {
    try {
        const user = await User.findOneByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.status(200).send({user, token});
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter(token => token.token !== req.token)
        req.user.save();
        res.status(200).send();
    } catch (error) {
        res.status(500).send({
            status: 500,
            error: error.message
        });
    }
})

router.get('/users/me', auth, async (req, res) => {
    await req.user.populate('tasks');
    res.status(200).send(req.user);
})

router.get('/users', async (req, res) => {
    try {
        const users = await User.find({})
        res.status(200).send(users);
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

router.get('/users/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const user = await User.findOne({_id: id})
        res.status(200).send(user);
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

router.post('/users', jsonParser, async (req, res) => {
    const email = req.body.email.toLowerCase();
    const password = req.body.password
    const name = req.body.name;
    const age = parseInt(req.body.age);
    const user = new User({email, password, name, age});
    try {
        await user.validate();
        await user.save()
        res.status(201).send(user);
    } catch (error) {
        res.status(400).send({
            status: 400,
            error: error.message
        });
    }
})

router.put('/users', jsonParser, async (req, res) => {
    try {
        let user = await User.findById(req.params.id);
        const updates = ['name', 'email', 'password', 'age'];
        updates.forEach(update => user[update] = req.body[update]);
        await user.save();
        res.status(202).send(user);
    } catch (error) {
        res.status(400).send({
            status: 400,
            error: error.message
        });
    }
})

router.delete('/users/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const user = await User.deleteOne({_id: id})
        if (!user)
            throw new Error("User not found");
        res.status(200).send();
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

export default router;
