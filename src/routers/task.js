import express from 'express';
import {Task} from "../models/task.js";
import bodyParser from "body-parser";
import {auth} from "../middleware/auth.js";

const router = new express.Router();
const jsonParser = bodyParser.json()

router.get('/tasks', auth, async (req, res) => {
    try {
        const tasks = await Task.find({owner: req.user.id})
        res.status(200).send(tasks);
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

router.get('/tasks/:id', auth, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await Task.findOne({_id: id})
        await task?.populate('owner');
        if (!task || task.owner.id !== req.user.id)
            throw new Error('Task not found');
        res.status(200).send(task);
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

router.post('/tasks', auth, jsonParser, async (req, res) => {
    const description = req.body.description
    const completed = req.body.completed;
    const task = new Task({
        description,
        completed,
        owner: req.user.id,
    });
    try {
        await task.save()
        res.status(201).send(task);
    } catch (error) {
        res.status(400).send({
            status: 400,
            error: error.message
        });
    }
})

router.put('/tasks', auth, jsonParser, async (req, res) => {
    const id = req.body.id;
    const description = req.body.description
    const completed = req.body.completed;
    try {
        const newTask = await Task.findOneAndUpdate(
            {_id: id, owner: req.user.id},
            {id, description, completed},
            {new: true});
        res.status(202).send(newTask);
    } catch (error) {
        res.status(400).send({
            status: 400,
            error: error.message
        });
    }
})

router.delete('/tasks/:id', auth, async (req, res) => {
    try {
        const id = req.params.id;
        const task = await Task.findOne({_id: id, owner: req.user.id})
        if (!task)
            throw new Error('Task not found');
        await Task.deleteOne({_id: id, owner: req.user.id})
        res.status(200).send();
    } catch (error) {
        res.status(404).send({
            status: 404,
            error: error.message
        });
    }
})

export default router;
