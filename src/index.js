import express from "express";
import usersRouter from './routers/user.js';
import tasksRouter from './routers/task.js';
import mongoose from "mongoose";

mongoose.connect('mongodb+srv://maingoose:maingoose@cluster0.4gamz.mongodb.net/?retryWrites=true&w=majority', {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    autoIndex: true,
})

const app = express();
const PORT = 3000;

app.use(usersRouter);
app.use(tasksRouter);

export const server = app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`)
})
