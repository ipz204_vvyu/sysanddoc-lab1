import jwt from "jsonwebtoken";
import {User} from "../models/user.js";

export const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization')?.replace('Bearer ', '');
        const decoded = jwt.verify(token, 'key');
        const user = await User.findOne({_id: decoded._id, 'tokens.token': token});
        if (!user) {
            throw new Error('Unauthorized');
        }
        req.user = user;
        req.token = token;
        next();
    } catch (error) {
        res.status(401).send({
            status: 401,
            error: error.message
        });
    }
}
